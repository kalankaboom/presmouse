#include "AnimationElement.hpp"

#include <boost/program_options.hpp>
#include <iostream>
#include <memory>
#include <queue>

constexpr unsigned int DEFAULT_DELAY = 200;

namespace po = boost::program_options;

[[noreturn]] void listenAndAnimate() {
    AnimationElement::init();

    std::queue<std::unique_ptr<AnimationElement>> animations;

    while (true) {
        XEvent event;
        AnimationElement::nextEvent(event);

        if (!animations.empty() && animations.back()->isDone()) {
            animations.pop();
        }

        if ((unsigned int)event.type == Button_Press_Type) {
            if (AnimationElement::queryButton() != BUTTON1) { continue; }

            animations.push(std::make_unique<AnimationElement>(
                event.xbutton.x_root, event.xbutton.y_root));
            animations.back()->start();
        } else if (!animations.empty() && !animations.back()->isReleased()) {
            if ((unsigned int)event.type == Button_Release_Type) {
                animations.back()->release();

            } else if ((unsigned int)event.type == Mouse_Move) [[likely]] {
                animations.back()->move(event.xbutton.x_root,
                                        event.xbutton.y_root);
            }
        }
    }
}

void animateAndDie(const unsigned int delay) {
    auto animation = std::make_unique<AnimationElement>();
    animation->start();
    std::this_thread::sleep_for(std::chrono::milliseconds(delay));
    animation->release();
    while (!animation->isDone()) {}
}

auto main(int argc, char **argv) -> int {
    po::options_description desc("Options");

    // clang-format off
    desc.add_options()
        ("help,h", "Show this help message")
        ("red,r", po::value<float>()->default_value(DEFAULT_COLOUR[0]),
            "The red component of the colour, between 0.0 and 1.0")
        ("green,g", po::value<float>()->default_value(DEFAULT_COLOUR[1]),
            "The green component of the colour, between 0.0 and 1.0")
        ("blue,b", po::value<float>()->default_value(DEFAULT_COLOUR[2]),
            "The blue component of the colour, between 0.0 and 1.0")
        ("opacity,o", po::value<float>()->default_value(DEFAULT_OPACITY),
            "The opacity of an individual disk, between 0.0 and 1.0")
        ("frames,f",
            po::value<unsigned int>()->default_value(DEFAULT_ANIMATION_FRAMES),
            "The number of animation frames")
        ("milliseconds,m",
            po::value<unsigned int>()->default_value(DEFAULT_MILLISECONDS),
            "The number of milliseconds between each frame")
        ("radius",
            po::value<unsigned int>()->default_value(DEFAULT_RADIUS),
            "The radius of the final circle")
        ("keybind",
            "Keybind mode. Use if you want to map presmouse to a keybinding. "
            "The cursor will only be highlighted when the binding is "
            "activated. Useful for applications that steal events (like "
            "Chromium).")
        ("delay",
            po::value<unsigned int>()->default_value(DEFAULT_DELAY),
            "Specifies the delay in milliseconds between the start of the "
            "animation and the start of the exit animation in keybind mode")
    ;
    // clang-format on

    po::variables_map cmd_opts;
    po::store(po::parse_command_line(argc, argv, desc), cmd_opts);
    po::notify(cmd_opts);

    if (cmd_opts.count("help") > 0) {
        std::cout << desc << std::endl;
        return 1;
    }

    if (cmd_opts.count("radius") > 0) {
        AnimationElement::setRadius(cmd_opts["radius"].as<unsigned int>());
    }

    if (cmd_opts.count("red") > 0 || cmd_opts.count("green") > 0 ||
        cmd_opts.count("blue") > 0 || cmd_opts.count("opacity") > 0) {
        const Colour colour = {cmd_opts["red"].as<float>(),
                               cmd_opts["green"].as<float>(),
                               cmd_opts["blue"].as<float>()};
        const float opacity = cmd_opts["opacity"].as<float>();
        AnimationElement::setColourAlpha(colour, opacity);
    }

    if (cmd_opts.count("frames") > 0 || cmd_opts.count("milliseconds") > 0) {
        AnimationElement::setFrameCountMilliseconds(
            cmd_opts["frames"].as<unsigned int>(),
            cmd_opts["milliseconds"].as<unsigned int>());
    }

    if (cmd_opts.count("keybind") <= 0) { listenAndAnimate(); }

    animateAndDie(cmd_opts["delay"].as<unsigned int>());
    AnimationElement::close();

    return 0;
}
