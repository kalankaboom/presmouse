#include "AnimationElement.hpp"

#include <X11/Xutil.h>
#include <chrono>
#include <iostream>

Display *const AnimationElement::Disp = XOpenDisplay(nullptr);
const Window AnimationElement::Root = DefaultRootWindow(Disp);
unsigned int AnimationElement::radius = DEFAULT_RADIUS;
unsigned int AnimationElement::width_height = DEFAULT_HEIGHT_WIDTH;
Colour AnimationElement::colour = DEFAULT_COLOUR;
float AnimationElement::opacity = DEFAULT_OPACITY;
unsigned int AnimationElement::frames = DEFAULT_ANIMATION_FRAMES;
unsigned int AnimationElement::milli = DEFAULT_MILLISECONDS;
float AnimationElement::growth = DEFAULT_GROWTH;

Window AnimationElement::root_return_discard = 0;
Window AnimationElement::child_return_discard = 0;
int AnimationElement::root_x_return_discard = 0;
int AnimationElement::root_y_return_discard = 0;
int AnimationElement::win_x_return_discard = 0;
int AnimationElement::win_y_return_discard = 0;

AnimationElement::AnimationElement()
    : AnimationElement(XFixesGetCursorImage(Disp)->x,
                       XFixesGetCursorImage(Disp)->y) {}

AnimationElement::AnimationElement(const unsigned int x_pos,
                                   const unsigned int y_pos) {
    XVisualInfo vinfo;

    if (XMatchVisualInfo(Disp, DefaultScreen(Disp), BITCOLOUR, TrueColor,
                         &vinfo) == 0) {
        std::cerr << "No visual found supporting 32 bit color, terminating"
                  << std::endl;
        _Exit(EXIT_FAILURE);
    }

    XSetWindowAttributes attrs;
    attrs.override_redirect = 1;

    attrs.colormap = XCreateColormap(Disp, Root, vinfo.visual, AllocNone);
    attrs.background_pixel = 0;
    attrs.border_pixel = 0;

    window = XCreateWindow(
        Disp, Root, (int)x_pos - (int)radius, (int)y_pos - (int)radius,
        width_height, width_height, 0, vinfo.depth, InputOutput, vinfo.visual,
        CWOverrideRedirect | CWColormap | CWBackPixel | CWBorderPixel, &attrs);

    XMapWindow(Disp, window);

    cairo_surface_t *surf = cairo_xlib_surface_create(
        Disp, window, vinfo.visual, (int)width_height, (int)width_height);
    cr_ctx = cairo_create(surf);
    cairo_surface_destroy(surf);
    cairo_set_source_rgba(cr_ctx, colour[0], colour[1], colour[2], opacity);
}

AnimationElement::~AnimationElement() {
    if (animation.joinable()) { animation.join(); }
    cairo_destroy(cr_ctx);
    XUnmapWindow(Disp, window);
    XDestroyWindow(Disp, window);
    XFlush(Disp);
}

void AnimationElement::applyColourAlpha() {
    cairo_set_source_rgba(cr_ctx, colour[0], colour[1], colour[2], opacity);
}

void AnimationElement::move(unsigned int x_pos, unsigned int y_pos) const {
    XMoveWindow(Disp, window, (int)x_pos - (int)radius,
                (int)y_pos - (int)radius);
}

void AnimationElement::release() { released = true; }

void AnimationElement::start() {
    animation = std::thread(&AnimationElement::animate, this);
}

[[nodiscard]] auto AnimationElement::isReleased() const -> bool {
    return released;
}
[[nodiscard]] auto AnimationElement::isDone() const -> bool { return done; }

void AnimationElement::drawDisk(const float minor_radius,
                                const unsigned int x_pos,
                                const unsigned int y_pos) {
    cairo_arc(cr_ctx, x_pos, y_pos, minor_radius, 0, PI_2);
    cairo_fill(cr_ctx);
}

void AnimationElement::animate() {
    unsigned int frame_in = 1;
    unsigned int frame_out = 1;
    const unsigned int last_frame = frames + 1;
    while (true) {
        if (frame_in <= frames) {
            cairo_set_operator(cr_ctx, CAIRO_OPERATOR_OVER);
            drawDisk((float)frame_in * growth, radius, radius);
            XFlush(Disp);
            ++frame_in;
        }
        if (released && frame_out <= last_frame) {
            cairo_set_operator(cr_ctx, CAIRO_OPERATOR_CLEAR);
            drawDisk((float)frame_out * growth, radius, radius);
            XFlush(Disp);
            ++frame_out;
        }
        if (frame_out == last_frame) {
            clear();
            done = true;
            return;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(milli));
    }
}

void AnimationElement::clear() {
    cairo_set_operator(cr_ctx, CAIRO_OPERATOR_CLEAR);
    cairo_rectangle(cr_ctx, 0, 0, width_height, width_height);
    cairo_fill(cr_ctx);
    cairo_set_operator(cr_ctx, CAIRO_OPERATOR_OVER);
    XFlush(Disp);
}
