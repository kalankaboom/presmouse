#pragma once
#include <X11/extensions/XInput.h>
#include <X11/extensions/XInput2.h>
#include <array>
#include <cairo/cairo-xlib.h>
#include <cmath>
#include <thread>
#include <vector>

using Colour = std::array<float, 3>;

const Colour DEFAULT_COLOUR = {0, 1, 1};
constexpr float DEFAULT_OPACITY = 0.25;
constexpr float PI_2 = M_PI * 2;
constexpr short int BITCOLOUR = 32;
constexpr unsigned int BUTTON1 = 256;
constexpr unsigned int DEFAULT_ANIMATION_FRAMES = 10;
constexpr unsigned int DEFAULT_HEIGHT_WIDTH = 200;
constexpr unsigned int DEFAULT_MILLISECONDS = 25;
constexpr unsigned int DEFAULT_RADIUS = DEFAULT_HEIGHT_WIDTH / 2;
constexpr float DEFAULT_GROWTH =
    (float)DEFAULT_RADIUS / (float)DEFAULT_ANIMATION_FRAMES;

static unsigned int Button_Press_Type = 0;
static unsigned int Button_Release_Type = 0;
static unsigned int Mouse_Move = 0;

class AnimationElement {
  public:
    AnimationElement();
    AnimationElement(unsigned int x_pos, unsigned int y_pos);
    AnimationElement(AnimationElement &&) = default;
    AnimationElement(const AnimationElement &) = delete;
    auto operator=(AnimationElement &&) -> AnimationElement & = default;
    auto operator=(const AnimationElement &) -> AnimationElement & = delete;
    ~AnimationElement();

    static void init() { snoopXinput(); }

    static void nextEvent(XEvent &event) { XNextEvent(Disp, &event); }

    static void setRadius(const unsigned int new_radius) {
        radius = new_radius;
        width_height = 2 * radius;
    }

    static void setColourAlpha(const Colour &new_colour,
                               const float new_opacity) {
        colour = new_colour;
        opacity = new_opacity;
    }

    static void setFrameCountMilliseconds(const unsigned int new_frames,
                                          const unsigned int new_milli) {
        frames = new_frames;
        milli = new_milli;
        growth = (float)radius / (float)frames;
    }

    static auto queryButton() -> unsigned int {
        unsigned int mask_return = 0;
        XQueryPointer(Disp, Root, &root_return_discard, &child_return_discard,
                      &root_x_return_discard, &root_y_return_discard,
                      &win_x_return_discard, &win_y_return_discard,
                      &mask_return);
        return mask_return;
    }

    static void close() { XCloseDisplay(Disp); }

    void applyColourAlpha();
    void move(unsigned int x_pos, unsigned int y_pos) const;
    void release();
    void start();

    [[nodiscard]] auto isReleased() const -> bool;
    [[nodiscard]] auto isDone() const -> bool;

  private:
    static void snoopXinput() {
        int numdevs = 0;
        XDevice *device = nullptr;

        XIEventMask evmask = {
            .deviceid = XIMasterPointer, .mask_len = 0, .mask = nullptr};
        XISelectEvents(Disp, Root, &evmask, 1);

        XDeviceInfo *const first_devinfo = XListInputDevices(Disp, &numdevs);
        XDeviceInfo *devinfo = first_devinfo;

        std::vector<XEventClass> event_list;

        for (int dev = 0; dev < numdevs; ++dev) {
            if (devinfo->use != IsXExtensionPointer) {
                ++devinfo;
                continue;
            }

            device = XOpenDevice(Disp, devinfo->id);
            if (device == nullptr) { break; }

            listenToDevice(device, devinfo, event_list);

            XCloseDevice(Disp, device);

            ++devinfo;
        }

        if (XSelectExtensionEvent(Disp, Root, event_list.data(),
                                  (int)event_list.size()) != 0) {
            if (first_devinfo != nullptr) { XFreeDeviceList(first_devinfo); }
            return;
        }
    }

    static void listenToDevice(XDevice *device, XDeviceInfo *devinfo,
                               std::vector<XEventClass> &event_list) {
        for (unsigned int dev_class = 0; (int)dev_class < devinfo->num_classes;
             ++dev_class) {

            if (device->classes->input_class == ButtonClass) {
                event_list.push_back(0);
                DeviceButtonPress(device, Button_Press_Type, event_list.back());

                event_list.push_back(0);
                DeviceButtonRelease(device, Button_Release_Type,
                                    event_list.back());

                event_list.push_back(0);
                DeviceMotionNotify(device, Mouse_Move, event_list.back())
            }
            ++device->classes;
        }
    }

    void drawDisk(float minor_radius, unsigned int x_pos, unsigned int y_pos);
    void animate();
    void clear();

    Window window;
    cairo_t *cr_ctx;
    std::thread animation;
    bool released = false;
    bool done = false;

    static Display *const Disp;
    static const Window Root;
    static unsigned int radius;
    static unsigned int width_height;
    static Colour colour;
    static float opacity;
    static unsigned int frames;
    static unsigned int milli;
    static float growth;

    static Window root_return_discard;
    static Window child_return_discard;
    static int root_x_return_discard;
    static int root_y_return_discard;
    static int win_x_return_discard;
    static int win_y_return_discard;
};
