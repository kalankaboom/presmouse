CC = g++
SRC	= AnimationElement.cpp presmouse.cpp
CFLAGS = -O3 -Wall -Wextra -std=c++23
LIBS = -lX11 -lXi -lXfixes -lcairo -lboost_program_options

PREFIX = /usr/local

all: presmouse

presmouse:
	$(CC) $(SRC) $(CFLAGS) $(LIBS) -o presmouse

install: all
	mkdir -p $(PREFIX)/bin
	cp -f presmouse $(PREFIX)/bin
	chmod 755 $(PREFIX)/bin/presmouse

uninstall:
	rm -f $(PREFIX)/bin/presmouse

clean:
	rm -f presmouse
