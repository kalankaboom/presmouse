# PresMouse : a mouse highlight utility

<h1 align="center">
  <img src="logo.svg" alt="PresMouse Logo">
</h1>

One day, I was watching a [Fireship](https://fireship.io/) video, and I was
inspired.
They have an effect on their mouse, which makes a nice little animation when
clicked.

I still don't know what tool they use for this, but I wanted to make my own.
A few headaches later, here we are : _PresMouse_.

This utility is specific to the [X Window System](https://x.org/wiki/).

## Dependencies

-   X11 (`libx11-dev`)
-   XInput (`libxi-dev`)
-   XFixes (`libxfixes-dev`)
-   [Cairo](https://www.cairographics.org/) (`libcairo2-dev`)
-   [Boost Program Options](https://www.boost.org/doc/libs/1_83_0/doc/html/program_options.html) (`libboost-program-options-dev`)

## Usage

Just run `presmouse` and start clicking !
